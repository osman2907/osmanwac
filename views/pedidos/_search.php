<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PedidosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pedidos-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'cliente_id') ?>

    <?= $form->field($model, 'formato_id') ?>

    <?= $form->field($model, 'tipo_id') ?>

    <?= $form->field($model, 'modalidad_id') ?>

    <?php // echo $form->field($model, 'nombre_proyecto') ?>

    <?php // echo $form->field($model, 'proposito_id') ?>

    <?php // echo $form->field($model, 'extension_id') ?>

    <?php // echo $form->field($model, 'idioma_id') ?>

    <?php // echo $form->field($model, 'optimizacion_seo') ?>

    <?php // echo $form->field($model, 'palabra_clave') ?>

    <?php // echo $form->field($model, 'palabras_secundarias') ?>

    <?php // echo $form->field($model, 'descripcion') ?>

    <?php // echo $form->field($model, 'archivo') ?>

    <?php // echo $form->field($model, 'grabacion') ?>

    <?php // echo $form->field($model, 'publico_objetivo') ?>

    <?php // echo $form->field($model, 'fecha_entrega') ?>

    <?php // echo $form->field($model, 'status_id') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
