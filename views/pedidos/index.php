<?php
use yii\helpers\Html;
use yii\grid\GridView;


$this->title = 'Pedidos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pedidos-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <br>
    <p>
        <?= Html::a('Registrar Pedidos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php
    $columnas=[
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute'=>'cliente_id',
            'label'=>'Cliente',
            'format'=>"raw",
            'filter'=>Yii::$app->user->identity->tipoUsuario == 1 ? $listClientes : false,
            'value'=>function($objeto,$clave,$index,$widget){
                return $objeto->cliente->apellido_nombre;
                
            },
        ],
        'nombre_proyecto',
        [
            'attribute'=>'fecha_entrega',
            'label'=>'Fecha de entrega',
            'format'=>"raw",
            'filter'=>false,
            'value'=>function($objeto,$clave,$index,$widget){
                return date("d/m/Y",strtotime($objeto->fecha_entrega));
            },
        ],
        [
            'attribute'=>'status_id',
            'label'=>'Estatus',
            'format'=>"raw",
            'filter'=>$listStatus,
            'value'=>function($objeto,$clave,$index,$widget){
                if($objeto->status_id == 1){
                    return "<span class='badge badge-success'>".$objeto->status->nombre."</span>";
                }else{
                    return "<span class='badge badge-danger'>".$objeto->status->nombre."</span>";
                }
                
            },
        ],
        [
            'label'=>'Acciones',
            'format'=>'raw',
            'value'=>function($objeto,$clave,$index,$widget){
                return $objeto->botonesAcciones;
                
            }
        ]
    ];
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $columnas
    ]); ?>


</div>

<?php
$script="   
    $(document).ready(function(){
        $('#menu-pedidos').addClass('active')
    });
";
$this->registerJs($script);
?>
