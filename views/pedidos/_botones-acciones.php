<?php
use yii\helpers\Url;
?>
<div class="btn-group  dropleft">
    <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Acciones <span class="caret"></span>
    </button>
    <ul class="dropdown-menu">
        <a href="<?= Url::to(['pedidos/view', 'id' => $model->id]); ?>">
            &nbsp;&nbsp;<span class="ti-search"></span>&nbsp;Consultar</a>
        <a href="<?= Url::to(['pedidos/update', 'id' => $model->id]); ?>">
            &nbsp;&nbsp;<span class="ti-pencil"></span>&nbsp;Modificar</a>
        <?php if($model->status_id == 1){ ?>
            <a href="<?= Url::to(['pedidos/deactivate', 'id' => $model->id]); ?>">
                &nbsp;&nbsp;<span class="ti-close"></span>&nbsp;Desactivar</a>
        <?php } ?>
        <?php if($model->status_id == 2){ ?>
            <a href="<?= Url::to(['pedidos/activate', 'id' => $model->id]); ?>">
                &nbsp;&nbsp;<span class="ti-check"></span>&nbsp;Activar</a>
        <?php } ?>
    </ul>
</div>

