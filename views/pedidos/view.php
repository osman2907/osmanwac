<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Pedidos */

$this->title = "Consultar pedido";
$this->params['breadcrumbs'][] = ['label' => 'Pedidos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="pedidos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Listado', ['index'], ['class' => 'btn btn-info']) ?>
        <?= Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php
        if($model->status_id == 1){
            echo Html::a('Desactivar', ['deactivate', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => '¿Seguro que desea desactivar este registro?',
                    'method' => 'post',
                ]
            ]);
        }
        ?>
        
        <?php
        if($model->status_id == 2){
            echo Html::a('Activar', ['activate', 'id' => $model->id], [
                'class' => 'btn btn-success',
                'data' => [
                    'confirm' => '¿Seguro que desea activar este registro?',
                    'method' => 'post',
                ]
            ]);
        }
        ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'label'=>'Cliente',
                'format'=>'raw',
                'value'=>function($objeto){
                    return $objeto->cliente->apellido_nombre;
                    
                }
            ],
            [
                'label'=>'Formato',
                'format'=>'raw',
                'value'=>function($objeto){
                    return $objeto->formato->nombre;
                    
                }
            ],
            [
                'label'=>'Tipo',
                'format'=>'raw',
                'value'=>function($objeto){
                    return $objeto->tipo->nombre;
                    
                }
            ],
            [
                'label'=>'Categorías',
                'format'=>'raw',
                'value'=>function($objeto){
                    $texto="<ul>";
                    foreach ($objeto->pedidosCategorias as  $modelPedidoCategoria) {
                        $texto.="<li>".$modelPedidoCategoria->categoria->nombre."</li>";
                    }
                    $texto.="</ul>";
                    
                    return $texto;
                }
            ],
            [
                'label'=>'Modalidad',
                'format'=>'raw',
                'value'=>function($objeto){
                    $texto="<span><b>".$objeto->modalidad->titulo."</b><span><br>";
                    $texto.="<span>".$objeto->modalidad->nombre."<span><br>";
                    return $texto;
                    
                }
            ],
            'nombre_proyecto',
            [
                'label'=>'Propósito',
                'format'=>'raw',
                'value'=>function($objeto){
                    $texto="<span><b>".$objeto->proposito->titulo."</b><span><br>";
                    $texto.="<span>".$objeto->proposito->nombre."<span><br>";
                    return $texto;
                    
                }
            ],
            [
                'label'=>'Extensión máxima de palabras',
                'format'=>'raw',
                'value'=>function($objeto){
                    return $objeto->extension->nombre;
                    
                }
            ],
            [
                'label'=>'Países a los que está dirigido',
                'format'=>'raw',
                'value'=>function($objeto){
                    $texto="<ul>";
                    foreach ($objeto->pedidosPaises as  $modelPedidoPais) {
                        $texto.="<li>".$modelPedidoPais->pais->nombre."</li>";
                    }
                    $texto.="</ul>";
                    
                    return $texto;
                }
            ],
            [
                'label'=>'Idioma',
                'format'=>'raw',
                'value'=>function($objeto){
                    return $objeto->idioma->nombre;
                    
                }
            ],
            'optimizacion_seo',
            'palabra_clave',
            'palabras_secundarias',
            'descripcion:ntext',
            'publico_objetivo:ntext',
            [
                'label'=>'Perspectiva del texto',
                'format'=>'raw',
                'value'=>function($objeto){
                    $texto="<span>".$objeto->perspectiva->nombre."<span><br>";
                    return $texto;
                    
                }
            ],
            [
                'label'=>'Fecha de entrega',
                'value'=>date("d/m/Y",strtotime($model->fecha_entrega))
            ],
            [
                'label'=>'Estatus',
                'format'=>'raw',
                'value'=>function($objeto){
                    if($objeto->status_id == 1){
                        return "<span class='badge badge-success'>".$objeto->status->nombre."</span>";
                    }else{
                        return "<span class='badge badge-danger'>".$objeto->status->nombre."</span>";
                    }
                }
            ],
            [
                'label'=>'Creado',
                'value'=>date("d/m/Y h:i:s a",strtotime($model->created_at))
            ],
            [
                'label'=>'Modificado',
                'value'=>date("d/m/Y h:i:s a",strtotime($model->updated_at))
            ]
        ],
    ]) ?>

</div>

<?php
$script="   
    $(document).ready(function(){
        $('#menu-pedidos').addClass('active')
    });
";
$this->registerJs($script);
?>
