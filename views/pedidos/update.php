<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pedidos */

$this->title = 'Modificación de pedido';
$this->params['breadcrumbs'][] = ['label' => 'Pedidos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pedidos-update">

    <div class="row formulario">
        <div class="col-md-4 offset-md-2">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
        'listFormatos' => $listFormatos,
        'listTipos' => $listTipos,
        'listCategorias' => $listCategorias,
        'listModalidades' => $listModalidades,
        'listPropositos' => $listPropositos,
        'listPerspectivas' => $listPerspectivas,
        'listExtensiones' => $listExtensiones,
        'listPaises' => $listPaises,
        'listIdiomas' => $listIdiomas,
        'listOptimizacion' => $listOptimizacion,
        'listClientes' => $listClientes
    ]) ?>

</div>

<?php
$script="   
    $(document).ready(function(){
        $('#menu-pedidos').addClass('active')
    });
";
$this->registerJs($script);
?>
