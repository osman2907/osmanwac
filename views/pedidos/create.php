<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pedidos */

$this->title = 'Creación de pedido';
$this->params['breadcrumbs'][] = ['label' => 'Pedidos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pedidos-create">

    <div class="row formulario">
        <div class="col-md-4 offset-md-2">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
        'listFormatos' => $listFormatos,
        'listTipos' => $listTipos,
        'listCategorias' => $listCategorias,
        'listModalidades' => $listModalidades,
        'listPropositos' => $listPropositos,
        'listExtensiones' => $listExtensiones,
        'listPaises' => $listPaises,
        'listIdiomas' => $listIdiomas,
        'listOptimizacion' => $listOptimizacion,
        'listClientes' => $listClientes,
        'listPerspectivas' => $listPerspectivas
    ]) ?>

</div>

<?php
$script="   
    $(document).ready(function(){
        $('#menu-pedidos').addClass('active')
    });
";
$this->registerJs($script);
?>
