<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\VarDumper;
use dosamigos\datepicker\DatePicker;
?>

<div class="pedidos-form">

    <?php
    /*echo "<pre>";
    print_r($listPerspectivas);
    echo "</pre>";*/
    ?>

    <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>

    <div class="row formulario">
        <div class="col-md-4 offset-md-2">
            <?= $form->field($model, 'formato_id')->dropDownList($listFormatos,['prompt'=>'SELECCIONE']) ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'tipo_id')->dropDownList($listTipos,['prompt'=>'SELECCIONE']) ?>
        </div>
    </div>

    <div class="row" style="margin-top: 15px;">
        <div class="col-md-8 offset-md-2">
            <?= $form->field($model, 'categorias')->dropDownList($listCategorias,['class'=>'multipleSelect','multiple'=>'multiple']) ?>
        </div>
    </div>

    <div class="row" style="margin-top: 30px;">
        <div class="col-md-8 offset-md-2">
            <label class="control-label">Modalidad deseada para tu pedido</label>
                <?=
                $form->field($model, 'modalidad_id')
                    ->radioList(
                        $listModalidades['registros'],
                        [
                            'item' => function($index, $label, $name, $checked, $value) {

                                $return = '<div class="col-md-6">';
                                    $return .= '<div class="caja-modalidad">';
                                        if($checked){
                                            $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" checked="checked">';
                                        }else{
                                            $return .= '<input type="radio" name="' . $name . '" value="' . $value . '">';
                                        }
                                        $return .= '<i></i>';
                                        $return .= ' <span>' . $label['titulo'] . '</span>';
                                        $return .= ' <br><span>' . $label['nombre'] . '</span>';
                                    $return .= '</div>';
                                $return .= '</div>';


                                return $return;
                            }
                        ]
                    )
                ->label(false);
                ?>
        </div>
    </div>

    <div class="row" style="margin-top: 40px;">
        <div class="col-md-8 offset-md-2">
           <?= $form->field($model, 'nombre_proyecto')->textInput(['maxlength' => true]) ?> 
        </div>
    </div>

    <div class="row" style="margin-top: 30px;">
        <div class="col-md-8 offset-md-2">
            <label class="control-label">Propósito del texto</label>
            <?=
                $form->field($model, 'proposito_id')
                    ->radioList(
                        $listPropositos['registros'],
                        [
                            'item' => function($index, $label, $name, $checked, $value) {

                                $return = '<div class="col-md-6">';
                                    $return .= '<div class="caja-proposito">';
                                        if($checked){
                                            $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" checked="checked">';
                                        }else{
                                            $return .= '<input type="radio" name="' . $name . '" value="' . $value . '">';
                                        }
                                        $return .= '<i></i>';
                                        $return .= ' <span>' . $label['titulo'] . '</span>';
                                        $return .= ' <br><span>' . $label['nombre'] . '</span>';
                                    $return .= '</div>';
                                $return .= '</div>';


                                return $return;
                            }
                        ]
                    )
                ->label(false);
                ?>
            
        </div>
    </div>

    <div class="row" style="margin-top: 30px;">
        <div class="col-md-4 offset-md-2">
            <?= $form->field($model, 'extension_id')->dropDownList($listExtensiones,['prompt'=>'SELECCIONE']) ?>
        </div>
    </div>

    <div class="row" style="margin-top: 45px;">
        <div class="col-md-8 offset-md-2">
            <?= $form->field($model, 'paises')->dropDownList($listPaises,['class'=>'multipleSelect','multiple'=>'multiple']) ?>
        </div>
    </div>

    <div class="row" style="margin-top: 30px;">
        <div class="col-md-4 offset-md-2">
            <?= $form->field($model, 'idioma_id')->dropDownList($listIdiomas,['prompt'=>'SELECCIONE']) ?>
        </div>
    </div>

    <div class="row" style="margin-top: 30px;">
        <div class="col-md-4 offset-md-2">
            <?= $form->field($model, 'optimizacion_seo')->dropDownList($listOptimizacion,[]) ?>
        </div>
    </div>

    <div class="row" style="margin-top: 30px;">
        <div class="col-md-8 offset-md-2">
           <?= $form->field($model, 'palabra_clave')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row" style="margin-top: 30px;">
        <div class="col-md-8 offset-md-2">
           <?= $form->field($model, 'palabras_secundarias')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row" style="margin-top: 30px;">
        <div class="col-md-8 offset-md-2">
           <?= $form->field($model, 'descripcion')->textarea(['rows' => 6]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3 offset-md-2">
           <?= Html::button("Adjunta archivo", ['class' => 'form-control btn-file']) ?>
        </div>

        <div class="col-md-3">
           <?= Html::button("Grabar instrucciones", ['class' => 'form-control btn-file']) ?>
        </div>
    </div>

    <div class="row" style="margin-top: 30px;">
        <div class="col-md-8 offset-md-2">
           <?= $form->field($model, 'publico_objetivo')->textarea(['rows' => 6]) ?>
        </div>
    </div>

    <div class="row" style="margin-top: 30px;">
        <div class="col-md-8 offset-md-2">
            <label class="control-label">Perspectiva del texto</label>
            <?=
                $form->field($model, 'perspectiva_id')
                    ->radioList(
                        $listPerspectivas['registros'],
                        [
                            'item' => function($index, $label, $name, $checked, $value) { 
                                $return="";
                                if(!isset($label['padre'])){
                                   $return .="&nbsp;&nbsp;&nbsp;&nbsp;"; 
                                }
                                $disabled="";
                                if(isset($label['padre']) && $label['padre']==1){
                                   $disabled="disabled"; 
                                }
                                if($checked){
                                    $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" checked="checked"'.$disabled.'>';
                                }else{
                                    $return .= '<input type="radio" name="' . $name . '" value="' . $value . '"'.$disabled.'>';
                                }
                                //$return .= '<i></i>';
                                $return .= ' <span>' . $label['nombre'] . '</span><br>';
                                        
                                    


                                return $return;
                            }
                        ]
                    )
                ->label(false);
                ?>
            
        </div>
    </div>

    <div class="row" style="margin-top: 30px;">
        <div class="col-md-4 offset-md-2">
           <?php
           //$form->field($model, 'fecha_entrega')->textInput(['maxlength' => true]) 
           ?>
           <?= $form->field($model, 'fecha_entrega')->widget(
                DatePicker::className(), [
                    // inline too, not bad
                    //'inline' => true,
                    'language' => 'es',
                     // modify template for custom rendering
                    'template' => '{input}{addon}',
                    'options' => ['autocomplete'=>"off",'readonly'=>"readonly"],
                    //'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'dd/mm/yyyy'
                    ]
            ]);?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8 offset-md-2">
            <?php
            if(Yii::$app->user->identity->tipoUsuario == 1){
                echo $form->field($model, 'cliente_id')->dropDownList($listClientes,['prompt'=>'SELECCIONE']);
            }else{
                echo $form->field($model, 'cliente_id')->hiddenInput()->label(false);
                echo $form->field($model, 'cliente_nombre')->textInput();
            }
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8 offset-md-2">
            <?= Html::submitButton($model->isNewRecord ? 'Crear pedido' : 'Modificar pedido', ['class' => 'btn btn-success pull-right']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
