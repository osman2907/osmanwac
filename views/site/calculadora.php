<?php

/* @var $this yii\web\View */

$this->title = 'Calculadora';
?>
<div class="site-index">
    <h1><?= $this->title ?></h1>
</div>

<?php
$script="	
	$(document).ready(function(){
		$('#menu-calculadora').addClass('active')
	});
";
$this->registerJs($script);
?>