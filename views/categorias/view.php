<?php
use yii\helpers\Html;
use yii\widgets\DetailView;


$this->title = "Consultar categoría";
$this->params['breadcrumbs'][] = ['label' => 'Categorias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="categorias-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <br>
    <p>
        <?= Html::a('Listado', ['index'], ['class' => 'btn btn-info']) ?>
        <?= Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php
        if($model->status_id == 1){
            echo Html::a('Desactivar', ['deactivate', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => '¿Seguro que desea desactivar este registro?',
                    'method' => 'post',
                ]
            ]);
        }
        ?>
        
        <?php
        if($model->status_id == 2){
            echo Html::a('Activar', ['activate', 'id' => $model->id], [
                'class' => 'btn btn-success',
                'data' => [
                    'confirm' => '¿Seguro que desea activar este registro?',
                    'method' => 'post',
                ]
            ]);
        }
        ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nombre',
            [
                'label'=>'Estatus',
                'format'=>'raw',
                'value'=>function($objeto){
                    if($objeto->status_id == 1){
                        return "<span class='badge badge-success'>".$objeto->status->nombre."</span>";
                    }else{
                        return "<span class='badge badge-danger'>".$objeto->status->nombre."</span>";
                    }
                }
            ],
            [
                'label'=>'Creado',
                'value'=>date("d/m/Y h:i:s a",strtotime($model->created_at))
            ],
             [
                'label'=>'Modificado',
                'value'=>date("d/m/Y h:i:s a",strtotime($model->updated_at))
            ]
            
        ],
    ]) ?>

</div>
