<?php
use yii\helpers\Html;
use yii\grid\GridView;


$this->title = 'Categorías';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categorias-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <br>
    <p>
        <?= Html::a('Registrar Categorías', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'nombre',
            [
                'attribute'=>'status_id',
                'label'=>'Estatus',
                'format'=>"raw",
                'filter'=>$listStatus,
                'value'=>function($objeto,$clave,$index,$widget){
                    if($objeto->status_id == 1){
                        return "<span class='badge badge-success'>".$objeto->status->nombre."</span>";
                    }else{
                        return "<span class='badge badge-danger'>".$objeto->status->nombre."</span>";
                    }
                    
                },
            ],
            [
                'label'=>'Acciones',
                'format'=>'raw',
                'value'=>function($objeto,$clave,$index,$widget){
                    return $objeto->botonesAcciones;
                    
                }
            ],

        ],
    ]); ?>


</div>
