<?php

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
?>

<?php $this->beginPage() ?>
<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <?php $this->registerCsrfMetaTags() ?>
    <title>Prueba WAC Osman Pérez</title>
    <?php $this->head() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <link rel="shortcut icon" type="image/png" href="assets/images/icon/favicon.ico"> -->
    <?php 
    $this->registerJsFile(
        '@web/js/modernizr-2.8.3.min.js'
    );?>
</head>

<body>
    <?php $this->beginBody() ?>
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- preloader area start -->
    <div id="preloader">
        <div class="loader"></div>
    </div>
    <!-- preloader area end -->
    <!-- page container area start -->
    <div class="page-container">
        <!-- sidebar menu area start -->
        <div class="sidebar-menu">
            <div class="sidebar-header">
                <div class="logo">
                    <!-- <a href="index.html"><img src="img/desarrolloWeb.jpg" alt="logo"></a> -->
                    <div class="circle"></div>
                </div>
            </div>
            <div class="main-menu">
                <div class="menu-inner">
                    <nav>
                        <ul class="metismenu" id="menu">
                            <?php if (!Yii::$app->user->isGuest) { ?>
                                <li id="menu-tupanel"><a href="<?= Url::to(['site/tupanel']); ?>"><i class="ti-user"></i> <span>Tu panel</span></a></li>
                                <li id="menu-notificaciones"><a href="<?= Url::to(['site/notificaciones']); ?>"><i class="ti-bell"></i> <span>Notificaciones</span></a></li>
                                <li id="menu-pedidos"><a href="<?= Url::to(['pedidos/index']); ?>"><i class="ti-book"></i> <span>Pedidos</span></a></li>
                                <li id="menu-cuenta"><a href="<?= Url::to(['site/cuenta']); ?>"><i class="ti-reload"></i> <span>Cuenta</span></a></li>
                                <li id="menu-calculadora"><a href="<?= Url::to(['site/calculadora']); ?>"><i class="ti-tablet"></i> <span>Calculadora</span></a></li>
                                <li id="menu-creditos"><a href="<?= Url::to(['site/creditos']); ?>"><i class="ti-folder"></i> <span>Créditos</span></a></li>
                                <li id="menu-seo"><a href="<?= Url::to(['site/seo']); ?>"><i class="ti-search"></i> <span>Análisis SEO</span></a></li>
                            <?php } ?>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        <!-- sidebar menu area end -->
        <!-- main content area start -->
        <div class="main-content">
            <!-- header area start -->
            <div class="header-area">
                <div class="row align-items-center">
                    <!-- nav and search button -->
                    <div class="col-md-6 col-sm-8 clearfix">
                        <div class="nav-btn pull-left">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>

                        <?php if (!Yii::$app->user->isGuest) { ?>
                            <span class="credits-head">Créditos disponibles: </span>
                            <span class="credits-head-value">50</span>
                        <?php } ?>
                    </div>
                    <!-- profile info & task notification -->
                    <div class="col-sm-6 clearfix">
                        <?php if (!Yii::$app->user->isGuest) { ?>
                            <div class="user-profile pull-right">
                                <ul class="notification-area">
                                    <li class="dropdown">
                                        <i class="ti-bell dropdown-toggle" data-toggle="dropdown">
                                        </i>
                                    </li>
                                </ul>
                                
                                <?= Html::img('@web/img/desarrolloWeb.jpg',['class'=>'avatar user-thumb','alt'=>'avatar']) ?>
                                <div class="user-name dropdown-toggle" data-toggle="dropdown">
                                    <div class="row">
                                        <div class="col-sm-9 user-head">
                                            Nombre usuario<br>
                                            <?= Yii::$app->user->identity->username ?>
                                        </div>
                                        <div class="col-sm-1">
                                            <i class="fa fa-angle-down"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="dropdown-menu">
                                    <?=
                                    Html::a('Cerrar Sesión',
                                        ['/site/logout'],
                                        ['class' => 'dropdown-item', 'data-method'=>'post']);
                                    ?>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <!-- header area end -->
            <div class="main-content-inner">
                <?= $content ?>
            </div>
        </div>
        <!-- main content area end -->
        <!-- footer area start-->
        <footer>
            <div class="footer-area">
                <p>Prueba WAC Osman Pérez</p>
            </div>
        </footer>
        <!-- footer area end-->
    </div>
    <?php $this->endBody() ?>

    <?php 
    $script="$('.multipleSelect').fastselect();"; 
    $this->registerJs($script);
    ?>
</body>

</html>
<?php $this->endPage() ?>
