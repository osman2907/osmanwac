<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "perspectivas".
 *
 * @property int $id
 * @property string $nombre
 * @property int $padre_id
 * @property int $status_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Status $status
 */
class Perspectivas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'perspectivas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'padre_id'], 'required'],
            [['padre_id', 'status_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['nombre'], 'string', 'max' => 100],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'padre_id' => 'Padre ID',
            'status_id' => 'Status ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function listPerspectivas(){
        $perspectivas=Perspectivas::find()->orderBy(['id'=>SORT_ASC])->asArray()->all();
        $retorno=[];
        foreach ($perspectivas as $perspectiva){
            $idPerspectiva=$perspectiva['id'];
            $retorno['registros'][$idPerspectiva]=$perspectiva;
            if($perspectiva['padre_id']){
                $idPadre=$perspectiva['padre_id'];
                $retorno['registros'][$idPadre]['padre']=1;
            }else{
                $retorno['registros'][$idPerspectiva]['padre']=0;
            }
        }
        return $retorno;
    }

    /**
     * Gets query for [[Status]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }
}
