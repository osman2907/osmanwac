<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pedidos".
 *
 * @property int $id
 * @property int $cliente_id
 * @property int $formato_id
 * @property int $tipo_id
 * @property int $modalidad_id
 * @property string $nombre_proyecto
 * @property int $proposito_id
 * @property int $extension_id
 * @property int $idioma_id
 * @property int $optimizacion_seo
 * @property string $palabra_clave
 * @property string $palabras_secundarias
 * @property string $descripcion
 * @property string|null $archivo
 * @property string|null $grabacion
 * @property string $publico_objetivo
 * @property int $perspectiva_id
 * @property string $fecha_entrega
 * @property int $status_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Status $status
 * @property Clientes $cliente
 * @property Extensiones $extension
 * @property Formatos $formato
 * @property Idiomas $idioma
 * @property Modalidades $modalidad
 * @property Tipos $tipo
 * @property Propositos $proposito
 * @property Perspectivas $perspectiva
 * @property PedidosCategorias[] $pedidosCategorias
 * @property PedidosPaises[] $pedidosPaises
 */

use yii\helpers\ArrayHelper;

class Pedidos extends \yii\db\ActiveRecord
{
    public $categorias;
    public $paises;
    public $cliente_nombre;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pedidos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cliente_id', 'formato_id', 'tipo_id', 'modalidad_id', 'nombre_proyecto', 'proposito_id', 'extension_id', 'idioma_id', 'palabra_clave', 'palabras_secundarias', 'descripcion', 'publico_objetivo', 'perspectiva_id','fecha_entrega','categorias','paises'], 'required'],
            [['cliente_id', 'formato_id', 'tipo_id', 'modalidad_id', 'proposito_id', 'extension_id', 'idioma_id', 'optimizacion_seo', 'perspectiva_id', 'status_id'], 'integer'],
            [['descripcion', 'publico_objetivo'], 'string'],
            [['fecha_entrega', 'created_at', 'updated_at'], 'safe'],
            [['nombre_proyecto', 'palabra_clave', 'palabras_secundarias', 'archivo', 'grabacion'], 'string', 'max' => 300],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status_id' => 'id']],
            [['cliente_id'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['cliente_id' => 'id']],
            [['extension_id'], 'exist', 'skipOnError' => true, 'targetClass' => Extensiones::className(), 'targetAttribute' => ['extension_id' => 'id']],
            [['formato_id'], 'exist', 'skipOnError' => true, 'targetClass' => Formatos::className(), 'targetAttribute' => ['formato_id' => 'id']],
            [['idioma_id'], 'exist', 'skipOnError' => true, 'targetClass' => Idiomas::className(), 'targetAttribute' => ['idioma_id' => 'id']],
            [['modalidad_id'], 'exist', 'skipOnError' => true, 'targetClass' => Modalidades::className(), 'targetAttribute' => ['modalidad_id' => 'id']],
            [['tipo_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tipos::className(), 'targetAttribute' => ['tipo_id' => 'id']],
            [['proposito_id'], 'exist', 'skipOnError' => true, 'targetClass' => Propositos::className(), 'targetAttribute' => ['proposito_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Id',
            'cliente_id' => 'Cliente',
            'cliente_nombre' => 'Cliente',
            'formato_id' => 'Formato',
            'tipo_id' => 'Tipo',
            'modalidad_id' => 'Modalidad',
            'nombre_proyecto' => 'Nombre de tu proyecto',
            'proposito_id' => 'Proposito del texto',
            'extension_id' => 'Extensión máxima de palabras',
            'idioma_id' => 'Idioma',
            'optimizacion_seo' => 'Optimizacion SEO',
            'palabra_clave' => 'Palabra clave principal',
            'palabras_secundarias' => 'Palabra(s) semánticas(s) o secundaria(s)',
            'descripcion' => 'Descripción del pedido',
            'archivo' => 'Archivo',
            'grabacion' => 'Grabacion',
            'publico_objetivo' => 'Público objetivo',
            'perspectiva_id' => 'Perspectiva',
            'fecha_entrega' => 'Fecha de entrega',
            'status_id' => 'Estatus',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function registrarPedido($valores){
        //Por realizar insert en múltiples tablas se utilizó el manejo transaccional por si algún sql falla.
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $fechaEntrega=explode("/", $this->fecha_entrega);
            $this->fecha_entrega=$fechaEntrega[2]."-".$fechaEntrega[1]."-".$fechaEntrega[0];
            $modelPedido = $this->save();
            $idPedido = $this->id;

            $categorias=$valores['Pedidos']['categorias'];
            $paises=$valores['Pedidos']['paises'];

            foreach ($categorias as $idCategoria) {
                $modelPedidoCategoria=new PedidosCategorias;
                $modelPedidoCategoria->pedido_id=$idPedido;
                $modelPedidoCategoria->categoria_id=$idCategoria;
                $modelPedidoCategoria->save();
            }

            foreach ($paises as $idPais) {
                $modelPedidoPais=new PedidosPaises;
                $modelPedidoPais->pedido_id=$idPedido;
                $modelPedidoPais->pais_id=$idPais;
                $modelPedidoPais->save();
            }

            $transaction->commit();
            return true;

        } catch (Exception $e) {
            $transaction->rollBack();
            return false;
        }
    }


    public function modificarPedido($valores){
        //Por realizar insert en múltiples tablas se utilizó el manejo transaccional por si algún sql falla.
        $transaction = Yii::$app->db->beginTransaction();
        try {

            $fechaEntrega=explode("/", $this->fecha_entrega);
            $this->fecha_entrega=$fechaEntrega[2]."-".$fechaEntrega[1]."-".$fechaEntrega[0];

            /*echo "<pre>";
            print_r($this->pedidosCategorias);
            echo "</pre>";
            die;*/

            $modelPedido = $this->save();
            $idPedido = $this->id;

            PedidosCategorias::deleteAll(['pedido_id' => $idPedido]);
            PedidosPaises::deleteAll(['pedido_id' => $idPedido]);

            $categorias=$valores['Pedidos']['categorias'];
            $paises=$valores['Pedidos']['paises'];

            foreach ($categorias as $idCategoria) {
                $modelPedidoCategoria=new PedidosCategorias;
                $modelPedidoCategoria->pedido_id=$idPedido;
                $modelPedidoCategoria->categoria_id=$idCategoria;
                $modelPedidoCategoria->save();
            }

            foreach ($paises as $idPais) {
                $modelPedidoPais=new PedidosPaises;
                $modelPedidoPais->pedido_id=$idPedido;
                $modelPedidoPais->pais_id=$idPais;
                $modelPedidoPais->save();
            }

            $transaction->commit();
            return true;

        } catch (Exception $e) {
            $transaction->rollBack();
            return false;
        }
    }


    public function categoriasSeleccionadas(){
        $categoriasSeleccionadas=$this->pedidosCategorias;
        $retorno=[];
        foreach($categoriasSeleccionadas as $categoria){
            $retorno[]=$categoria->categoria_id;
        }
        return $retorno;
    }

    public function paisesSeleccionados(){
        $paisesSeleccionados=$this->pedidosPaises;
        $retorno=[];
        foreach($paisesSeleccionados as $pais){
            $retorno[]=$pais->pais_id;
        }
        return $retorno;
    }


    public function getBotonesAcciones(){
        $model=$this;
        return Yii::$app->controller->renderPartial('_botones-acciones',compact('model'));
    }

    

    /**
     * Gets query for [[Status]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * Gets query for [[Cliente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCliente()
    {
        return $this->hasOne(Clientes::className(), ['id' => 'cliente_id']);
    }

    /**
     * Gets query for [[Extension]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getExtension()
    {
        return $this->hasOne(Extensiones::className(), ['id' => 'extension_id']);
    }

    /**
     * Gets query for [[Formato]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFormato()
    {
        return $this->hasOne(Formatos::className(), ['id' => 'formato_id']);
    }

    /**
     * Gets query for [[Idioma]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdioma()
    {
        return $this->hasOne(Idiomas::className(), ['id' => 'idioma_id']);
    }

    /**
     * Gets query for [[Modalidad]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getModalidad()
    {
        return $this->hasOne(Modalidades::className(), ['id' => 'modalidad_id']);
    }

    /**
     * Gets query for [[Tipo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTipo()
    {
        return $this->hasOne(Tipos::className(), ['id' => 'tipo_id']);
    }

    /**
     * Gets query for [[Proposito]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProposito()
    {
        return $this->hasOne(Propositos::className(), ['id' => 'proposito_id']);
    }

    public function getPerspectiva()
    {
        return $this->hasOne(Perspectivas::className(), ['id' => 'perspectiva_id']);
    }

    /**
     * Gets query for [[PedidosCategorias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPedidosCategorias()
    {
        return $this->hasMany(PedidosCategorias::className(), ['pedido_id' => 'id']);
    }

    /**
     * Gets query for [[PedidosPaises]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPedidosPaises()
    {
        return $this->hasMany(PedidosPaises::className(), ['pedido_id' => 'id']);
    }
}
