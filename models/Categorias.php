<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

class Categorias extends \yii\db\ActiveRecord{
    
    public static function tableName(){
        return 'categorias';
    }

    
    public function rules(){
        return [
            [['nombre'], 'required','message'=>'Por favor ingrese {attribute}.'],
            [['status_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['nombre'], 'string', 'max' => 100],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status_id' => 'id']],
        ];
    }

    
    public function attributeLabels(){
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'status_id' => 'Estatus',
            'created_at' => 'Creado',
            'updated_at' => 'Modificado',
        ];
    }

    public function listCategorias(){
        $categorias=Categorias::find()->orderBy(['nombre'=>SORT_ASC])->all();
        $listCategorias=ArrayHelper::map($categorias,'id','nombre');
        return $listCategorias;
    }

    
    public function getStatus(){
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    
    public function getPedidosCategorias(){
        return $this->hasMany(PedidosCategorias::className(), ['categoria_id' => 'id']);
    }

    public function getBotonesAcciones(){
        $model=$this;
        return Yii::$app->controller->renderPartial('_botones-acciones',compact('model'));
    }

}
