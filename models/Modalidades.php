<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "modalidades".
 *
 * @property int $id
 * @property string $titulo
 * @property string $nombre
 * @property int $status_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Status $status
 * @property Pedidos[] $pedidos
 */
class Modalidades extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'modalidades';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['titulo', 'nombre'], 'required'],
            [['status_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['titulo'], 'string', 'max' => 200],
            [['nombre'], 'string', 'max' => 500],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'titulo' => 'Titulo',
            'nombre' => 'Nombre',
            'status_id' => 'Status ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function listModalidades(){
        $modalidades=Modalidades::find()->orderBy(['nombre'=>SORT_ASC])->asArray()->all();
        $retorno=[];
        foreach ($modalidades as $modalidad){
            $idModalidad=$modalidad['id'];
            $retorno['registros'][$idModalidad]=$modalidad;
        }
        return $retorno;
    }

    /**
     * Gets query for [[Status]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * Gets query for [[Pedidos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPedidos()
    {
        return $this->hasMany(Pedidos::className(), ['modalidad_id' => 'id']);
    }
}
