<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

class Status extends \yii\db\ActiveRecord{

    public static function tableName(){
        return 'status';
    }

   
    public function rules(){
        return [
            [['id', 'nombre'], 'required'],
            [['id'], 'integer'],
            [['nombre'], 'string', 'max' => 100],
            [['id'], 'unique'],
        ];
    }

    
    public function attributeLabels(){
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
        ];
    }

    public function listStatus(){
        $status=Status::find()->orderBy(['nombre'=>SORT_ASC])->all();
        $listStatus=ArrayHelper::map($status,'id','nombre');
        return $listStatus;
    }


    
    public function getCategorias(){
        return $this->hasMany(Categorias::className(), ['status_id' => 'id']);
    }

    
    public function getClientes(){
        return $this->hasMany(Clientes::className(), ['status_id' => 'id']);
    }

    
    public function getExtensiones(){
        return $this->hasMany(Extensiones::className(), ['status_id' => 'id']);
    }

    
    public function getFormatos(){
        return $this->hasMany(Formatos::className(), ['status_id' => 'id']);
    }

    
    public function getIdiomas(){
        return $this->hasMany(Idiomas::className(), ['status_id' => 'id']);
    }

    
    public function getModalidades(){
        return $this->hasMany(Modalidades::className(), ['status_id' => 'id']);
    }

    
    public function getPaises(){
        return $this->hasMany(Paises::className(), ['status_id' => 'id']);
    }

    
    public function getPedidos(){
        return $this->hasMany(Pedidos::className(), ['status_id' => 'id']);
    }

    
    public function getPerspectivas(){
        return $this->hasMany(Perspectivas::className(), ['status_id' => 'id']);
    }

    
    public function getPropositos(){
        return $this->hasMany(Propositos::className(), ['status_id' => 'id']);
    }

    
    public function getTipos(){
        return $this->hasMany(Tipos::className(), ['status_id' => 'id']);
    }


    public function getUsuarios(){
        return $this->hasMany(Usuarios::className(), ['status_id' => 'id']);
    }
}
