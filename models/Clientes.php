<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "clientes".
 *
 * @property int $id
 * @property int $cedula
 * @property string $primer_nombre
 * @property string|null $segundo_nombre
 * @property string $primer_apellido
 * @property string|null $segundo_apellido
 * @property string $teléfono
 * @property string $correo_electronico
 * @property int $status_id
 * @property int $usuario_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Status $status
 * @property Usuarios $usuario
 * @property Pedidos[] $pedidos
 */
class Clientes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clientes';
    }

    public function getApellido_nombre()
   {
      return $this->primer_apellido . " " . $this->primer_nombre;
   }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cedula', 'primer_nombre', 'primer_apellido', 'teléfono', 'correo_electronico', 'usuario_id'], 'required'],
            [['cedula', 'status_id', 'usuario_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['primer_nombre', 'segundo_nombre', 'primer_apellido', 'segundo_apellido'], 'string', 'max' => 100],
            [['teléfono'], 'string', 'max' => 30],
            [['correo_electronico'], 'string', 'max' => 300],
            [['cedula'], 'unique'],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status_id' => 'id']],
            [['usuario_id'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::className(), 'targetAttribute' => ['usuario_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cedula' => 'Cedula',
            'primer_nombre' => 'Primer Nombre',
            'segundo_nombre' => 'Segundo Nombre',
            'primer_apellido' => 'Primer Apellido',
            'segundo_apellido' => 'Segundo Apellido',
            'teléfono' => 'Teléfono',
            'correo_electronico' => 'Correo Electronico',
            'status_id' => 'Status ID',
            'usuario_id' => 'Usuario ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function listClientes(){
        $clientes=Clientes::find()->orderBy(['primer_apellido'=>SORT_ASC])->all();
        $listClientes=ArrayHelper::map($clientes,'id','apellido_nombre');
        return $listClientes;
    }

    public function clienteConectado(){
        $idUsuario=Yii::$app->user->identity->id;
        $cliente=Clientes::find()->where(['usuario_id'=>$idUsuario])->asArray()->one();
        return $cliente;
    }

    /**
     * Gets query for [[Status]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * Gets query for [[Usuario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuarios::className(), ['id' => 'usuario_id']);
    }

    /**
     * Gets query for [[Pedidos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPedidos()
    {
        return $this->hasMany(Pedidos::className(), ['cliente_id' => 'id']);
    }
}
