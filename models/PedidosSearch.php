<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Pedidos;
use Yii;

/**
 * PedidosSearch represents the model behind the search form of `app\models\Pedidos`.
 */
class PedidosSearch extends Pedidos
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'cliente_id', 'formato_id', 'tipo_id', 'modalidad_id', 'proposito_id','perspectiva_id', 'extension_id', 'idioma_id', 'optimizacion_seo', 'status_id'], 'integer'],
            [['nombre_proyecto', 'palabra_clave', 'palabras_secundarias', 'descripcion', 'archivo', 'grabacion', 'publico_objetivo', 'fecha_entrega', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pedidos::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if(Yii::$app->user->identity->tipoUsuario != 1){
            $clienteConectado=Clientes::clienteConectado();
            $this->cliente_id=$clienteConectado['id'];
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'cliente_id' => $this->cliente_id,
            'formato_id' => $this->formato_id,
            'tipo_id' => $this->tipo_id,
            'modalidad_id' => $this->modalidad_id,
            'proposito_id' => $this->proposito_id,
            'proposito_id' => $this->proposito_id,
            'perspectiva_id' => $this->perspectiva_id,
            'idioma_id' => $this->idioma_id,
            'optimizacion_seo' => $this->optimizacion_seo,
            'fecha_entrega' => $this->fecha_entrega,
            'status_id' => $this->status_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'nombre_proyecto', $this->nombre_proyecto])
            ->andFilterWhere(['like', 'palabra_clave', $this->palabra_clave])
            ->andFilterWhere(['like', 'palabras_secundarias', $this->palabras_secundarias])
            ->andFilterWhere(['like', 'descripcion', $this->descripcion])
            ->andFilterWhere(['like', 'archivo', $this->archivo])
            ->andFilterWhere(['like', 'grabacion', $this->grabacion])
            ->andFilterWhere(['like', 'publico_objetivo', $this->publico_objetivo]);

        return $dataProvider;
    }
}
