<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Categorias;


class CategoriasSearch extends Categorias{
    
    public function rules(){
        return [
            [['id', 'status_id'], 'integer'],
            [['nombre', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    
    public function scenarios(){
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
    
    
    public function search($params){
        $query = Categorias::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status_id' => $this->status_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre]);

        return $dataProvider;
    }
}
