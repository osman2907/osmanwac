-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-08-2020 a las 09:14:25
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `osmanwac`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `status_id` int(11) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id`, `nombre`, `status_id`, `created_at`, `updated_at`) VALUES
(1, 'Arte', 1, '2020-08-07 23:22:41', '2020-08-07 23:22:41'),
(2, 'Cultura', 1, '2020-08-07 23:23:26', '2020-08-07 23:23:26'),
(3, 'Estilo de vida', 2, '2020-08-07 23:23:40', '2020-08-07 23:23:40'),
(4, 'Música', 1, '2020-08-08 07:22:21', '2020-08-08 07:22:21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `id` int(11) NOT NULL,
  `cedula` bigint(20) NOT NULL,
  `primer_nombre` varchar(100) NOT NULL,
  `segundo_nombre` varchar(100) DEFAULT NULL,
  `primer_apellido` varchar(100) NOT NULL,
  `segundo_apellido` varchar(100) DEFAULT NULL,
  `teléfono` varchar(30) NOT NULL,
  `correo_electronico` varchar(300) NOT NULL,
  `status_id` int(11) NOT NULL DEFAULT 1,
  `usuario_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id`, `cedula`, `primer_nombre`, `segundo_nombre`, `primer_apellido`, `segundo_apellido`, `teléfono`, `correo_electronico`, `status_id`, `usuario_id`, `created_at`, `updated_at`) VALUES
(1, 1127630858, 'Osman', 'Orlando', 'Pérez', 'Martínez', '+584263063803', 'osman2907@gmail.com', 1, 100, '2020-08-09 02:35:39', '2020-08-09 02:35:39'),
(3, 1127630850, 'Carlos', 'José', 'Mora', 'Rodríguez', '+584248569328', 'cmora@gmail.com', 1, 101, '2020-08-09 02:35:39', '2020-08-09 02:35:39'),
(5, 1127630851, 'María', 'Josefina', 'González', 'Parra', '+584248569999', 'mgonzalez@gmail.com', 1, 102, '2020-08-09 02:35:39', '2020-08-09 02:35:39');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `extensiones`
--

CREATE TABLE `extensiones` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `status_id` int(11) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `extensiones`
--

INSERT INTO `extensiones` (`id`, `nombre`, `status_id`, `created_at`, `updated_at`) VALUES
(1, ' 250 a 499', 1, '2020-08-09 06:46:57', '2020-08-09 06:46:57');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `formatos`
--

CREATE TABLE `formatos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `status_id` int(11) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `formatos`
--

INSERT INTO `formatos` (`id`, `nombre`, `status_id`, `created_at`, `updated_at`) VALUES
(1, 'Texto', 1, '2020-08-09 05:10:41', '2020-08-09 05:10:41');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `idiomas`
--

CREATE TABLE `idiomas` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `status_id` int(11) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `idiomas`
--

INSERT INTO `idiomas` (`id`, `nombre`, `status_id`, `created_at`, `updated_at`) VALUES
(1, 'Español', 1, '2020-08-09 06:57:59', '2020-08-09 06:57:59');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modalidades`
--

CREATE TABLE `modalidades` (
  `id` int(11) NOT NULL,
  `titulo` varchar(200) NOT NULL,
  `nombre` varchar(500) NOT NULL,
  `status_id` int(11) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `modalidades`
--

INSERT INTO `modalidades` (`id`, `titulo`, `nombre`, `status_id`, `created_at`, `updated_at`) VALUES
(1, 'Concurso', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa', 1, '2020-08-09 06:07:39', '2020-08-09 06:07:39'),
(2, 'Proveedor favorito', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa', 1, '2020-08-09 06:08:01', '2020-08-09 06:08:01');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paises`
--

CREATE TABLE `paises` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `status_id` int(11) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `paises`
--

INSERT INTO `paises` (`id`, `nombre`, `status_id`, `created_at`, `updated_at`) VALUES
(1, 'Colombia', 1, '2020-08-09 06:53:25', '2020-08-09 06:53:25'),
(2, 'Ecuador', 1, '2020-08-09 06:53:29', '2020-08-09 06:53:29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedidos`
--

CREATE TABLE `pedidos` (
  `id` int(11) NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `formato_id` int(11) NOT NULL,
  `tipo_id` int(11) NOT NULL,
  `modalidad_id` int(11) NOT NULL,
  `nombre_proyecto` varchar(300) NOT NULL,
  `proposito_id` int(11) NOT NULL,
  `extension_id` int(11) NOT NULL,
  `idioma_id` int(11) NOT NULL,
  `optimizacion_seo` int(11) NOT NULL DEFAULT 1,
  `palabra_clave` varchar(300) NOT NULL,
  `palabras_secundarias` varchar(300) NOT NULL,
  `descripcion` text NOT NULL,
  `archivo` varchar(300) DEFAULT NULL,
  `grabacion` varchar(300) DEFAULT NULL,
  `publico_objetivo` text NOT NULL,
  `perspectiva_id` int(11) NOT NULL DEFAULT 1,
  `fecha_entrega` date NOT NULL,
  `status_id` int(11) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `pedidos`
--

INSERT INTO `pedidos` (`id`, `cliente_id`, `formato_id`, `tipo_id`, `modalidad_id`, `nombre_proyecto`, `proposito_id`, `extension_id`, `idioma_id`, `optimizacion_seo`, `palabra_clave`, `palabras_secundarias`, `descripcion`, `archivo`, `grabacion`, `publico_objetivo`, `perspectiva_id`, `fecha_entrega`, `status_id`, `created_at`, `updated_at`) VALUES
(18, 1, 1, 1, 1, 'Nombre', 1, 1, 1, 1, 'Ninguna', 'Otra ninguna', 'Probando', NULL, NULL, 'Ninguno', 1, '2020-08-09', 1, '2020-08-09 07:57:44', '2020-08-09 07:57:44'),
(20, 3, 1, 1, 1, 'Proyecto2', 1, 1, 1, 1, 'Palabra', 'Secundaria', 'Pruebas', NULL, NULL, 'Objetivo', 1, '2020-08-15', 1, '2020-08-09 21:51:15', '2020-08-09 21:51:15'),
(21, 3, 1, 1, 1, 'Prueba', 1, 1, 1, 0, 'Ninguna', 'Otra ninguna', 'Pruebas', NULL, NULL, 'Ninguno', 4, '2020-08-22', 1, '2020-08-10 01:34:11', '2020-08-10 01:34:11'),
(22, 3, 1, 1, 1, 'Prueba', 1, 1, 1, 1, 'Ninguna', 'Otra ninguna', 'Ninguno', NULL, NULL, 'Ninguno', 4, '2020-08-12', 1, '2020-08-10 01:35:57', '2020-08-10 01:35:57'),
(23, 5, 1, 1, 1, 'Proyecto María', 2, 1, 1, 1, 'Maria', 'González', 'Publicar mi proyecto', NULL, NULL, 'Profesionales en la gestión ambiental.', 4, '2020-08-30', 2, '2020-08-10 02:06:39', '2020-08-10 02:06:39');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedidos_categorias`
--

CREATE TABLE `pedidos_categorias` (
  `id` int(11) NOT NULL,
  `pedido_id` int(11) NOT NULL,
  `categoria_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `pedidos_categorias`
--

INSERT INTO `pedidos_categorias` (`id`, `pedido_id`, `categoria_id`, `created_at`, `updated_at`) VALUES
(13, 18, 1, '2020-08-09 07:57:44', '2020-08-09 07:57:44'),
(14, 18, 2, '2020-08-09 07:57:44', '2020-08-09 07:57:44'),
(15, 18, 4, '2020-08-09 07:57:44', '2020-08-09 07:57:44'),
(30, 20, 1, '2020-08-10 00:06:21', '2020-08-10 00:06:21'),
(31, 20, 3, '2020-08-10 00:06:21', '2020-08-10 00:06:21'),
(32, 21, 1, '2020-08-10 01:34:11', '2020-08-10 01:34:11'),
(33, 22, 2, '2020-08-10 01:35:57', '2020-08-10 01:35:57'),
(36, 23, 1, '2020-08-10 02:07:19', '2020-08-10 02:07:19'),
(37, 23, 3, '2020-08-10 02:07:20', '2020-08-10 02:07:20');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedidos_paises`
--

CREATE TABLE `pedidos_paises` (
  `id` int(11) NOT NULL,
  `pedido_id` int(11) NOT NULL,
  `pais_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `pedidos_paises`
--

INSERT INTO `pedidos_paises` (`id`, `pedido_id`, `pais_id`, `created_at`, `updated_at`) VALUES
(5, 18, 1, '2020-08-09 07:57:45', '2020-08-09 07:57:45'),
(21, 20, 2, '2020-08-10 00:06:21', '2020-08-10 00:06:21'),
(22, 21, 1, '2020-08-10 01:34:12', '2020-08-10 01:34:12'),
(23, 22, 1, '2020-08-10 01:35:58', '2020-08-10 01:35:58'),
(26, 23, 1, '2020-08-10 02:07:20', '2020-08-10 02:07:20'),
(27, 23, 2, '2020-08-10 02:07:20', '2020-08-10 02:07:20');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perspectivas`
--

CREATE TABLE `perspectivas` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `padre_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `perspectivas`
--

INSERT INTO `perspectivas` (`id`, `nombre`, `padre_id`, `status_id`, `created_at`, `updated_at`) VALUES
(1, 'Primer persona del singular(yo, mí, mi)', 0, 1, '2020-08-10 00:51:11', '2020-08-10 00:51:11'),
(2, 'Primer persona del plural(nosotros, nuestro(s), nuestra(s))', 0, 1, '2020-08-10 00:51:11', '2020-08-10 00:51:11'),
(3, 'Segunda persona, formal o informal', 0, 1, '2020-08-10 00:51:11', '2020-08-10 00:51:11'),
(4, 'Usted, su, sus', 3, 1, '2020-08-10 00:51:11', '2020-08-10 00:51:11'),
(5, 'Tú, tu, tus', 3, 1, '2020-08-10 00:51:11', '2020-08-10 00:51:11'),
(6, 'Tercera persona(ellos, ellas, su(s))', 0, 1, '2020-08-10 00:51:11', '2020-08-10 00:51:11');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `propositos`
--

CREATE TABLE `propositos` (
  `id` int(11) NOT NULL,
  `titulo` varchar(200) NOT NULL,
  `nombre` varchar(500) NOT NULL,
  `status_id` int(11) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `propositos`
--

INSERT INTO `propositos` (`id`, `titulo`, `nombre`, `status_id`, `created_at`, `updated_at`) VALUES
(1, 'Contenido de blog', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. ', 1, '2020-08-09 06:38:57', '2020-08-09 06:38:57'),
(2, 'Guiones', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. ', 1, '2020-08-09 06:39:08', '2020-08-09 06:39:08');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `status`
--

CREATE TABLE `status` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `status`
--

INSERT INTO `status` (`id`, `nombre`) VALUES
(1, 'Activo'),
(2, 'Inactivo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipos`
--

CREATE TABLE `tipos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `status_id` int(11) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tipos`
--

INSERT INTO `tipos` (`id`, `nombre`, `status_id`, `created_at`, `updated_at`) VALUES
(1, 'Texto especializado', 1, '2020-08-09 05:11:09', '2020-08-09 05:11:09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `usuario` varchar(100) NOT NULL,
  `contrasena` varchar(100) NOT NULL,
  `usuario_tipo_id` int(11) NOT NULL DEFAULT 2,
  `status_id` int(11) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `usuario`, `contrasena`, `usuario_tipo_id`, `status_id`, `created_at`, `updated_at`) VALUES
(100, 'operez', '12345678', 1, 1, '2020-08-09 02:35:29', '2020-08-09 02:35:29'),
(101, 'cmora', '12345678', 1, 1, '2020-08-09 02:35:29', '2020-08-09 02:35:29'),
(102, 'mgonzalez', '12345678', 1, 1, '2020-08-09 02:35:29', '2020-08-09 02:35:29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios_tipos`
--

CREATE TABLE `usuarios_tipos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `status_id` int(11) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios_tipos`
--

INSERT INTO `usuarios_tipos` (`id`, `nombre`, `status_id`, `created_at`, `updated_at`) VALUES
(1, 'Administrador', 1, '2020-08-09 02:42:25', '2020-08-09 02:42:25'),
(2, 'Cliente', 1, '2020-08-09 02:42:34', '2020-08-09 02:42:34');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status_id` (`status_id`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cedula` (`cedula`),
  ADD KEY `status_id` (`status_id`),
  ADD KEY `usuario_id` (`usuario_id`);

--
-- Indices de la tabla `extensiones`
--
ALTER TABLE `extensiones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status_id` (`status_id`);

--
-- Indices de la tabla `formatos`
--
ALTER TABLE `formatos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status_id` (`status_id`);

--
-- Indices de la tabla `idiomas`
--
ALTER TABLE `idiomas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status_id` (`status_id`);

--
-- Indices de la tabla `modalidades`
--
ALTER TABLE `modalidades`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status_id` (`status_id`);

--
-- Indices de la tabla `paises`
--
ALTER TABLE `paises`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status_id` (`status_id`);

--
-- Indices de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status_id` (`status_id`),
  ADD KEY `cliente_id` (`cliente_id`),
  ADD KEY `extension_id` (`extension_id`),
  ADD KEY `formato_id` (`formato_id`),
  ADD KEY `idioma_id` (`idioma_id`),
  ADD KEY `modalidad_id` (`modalidad_id`),
  ADD KEY `tipo_id` (`tipo_id`),
  ADD KEY `proposito_id` (`proposito_id`),
  ADD KEY `pedidos_fk` (`perspectiva_id`);

--
-- Indices de la tabla `pedidos_categorias`
--
ALTER TABLE `pedidos_categorias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pedido_id` (`pedido_id`),
  ADD KEY `categoria_id` (`categoria_id`);

--
-- Indices de la tabla `pedidos_paises`
--
ALTER TABLE `pedidos_paises`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pedido_id` (`pedido_id`),
  ADD KEY `pais_id` (`pais_id`);

--
-- Indices de la tabla `perspectivas`
--
ALTER TABLE `perspectivas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status_id` (`status_id`);

--
-- Indices de la tabla `propositos`
--
ALTER TABLE `propositos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status_id` (`status_id`);

--
-- Indices de la tabla `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipos`
--
ALTER TABLE `tipos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status_id` (`status_id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status_id` (`status_id`);

--
-- Indices de la tabla `usuarios_tipos`
--
ALTER TABLE `usuarios_tipos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status_id` (`status_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `extensiones`
--
ALTER TABLE `extensiones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `formatos`
--
ALTER TABLE `formatos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `idiomas`
--
ALTER TABLE `idiomas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `modalidades`
--
ALTER TABLE `modalidades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `paises`
--
ALTER TABLE `paises`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de la tabla `pedidos_categorias`
--
ALTER TABLE `pedidos_categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT de la tabla `pedidos_paises`
--
ALTER TABLE `pedidos_paises`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT de la tabla `perspectivas`
--
ALTER TABLE `perspectivas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `propositos`
--
ALTER TABLE `propositos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tipos`
--
ALTER TABLE `tipos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT de la tabla `usuarios_tipos`
--
ALTER TABLE `usuarios_tipos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD CONSTRAINT `categorias_ibfk_1` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD CONSTRAINT `clientes_ibfk_1` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `clientes_ibfk_2` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`id`);

--
-- Filtros para la tabla `extensiones`
--
ALTER TABLE `extensiones`
  ADD CONSTRAINT `extensiones_ibfk_1` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `formatos`
--
ALTER TABLE `formatos`
  ADD CONSTRAINT `formatos_ibfk_1` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `idiomas`
--
ALTER TABLE `idiomas`
  ADD CONSTRAINT `idiomas_ibfk_1` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `modalidades`
--
ALTER TABLE `modalidades`
  ADD CONSTRAINT `modalidades_ibfk_1` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `paises`
--
ALTER TABLE `paises`
  ADD CONSTRAINT `paises_ibfk_1` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `pedidos`
--
ALTER TABLE `pedidos`
  ADD CONSTRAINT `pedidos_fk` FOREIGN KEY (`perspectiva_id`) REFERENCES `perspectivas` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pedidos_ibfk_1` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pedidos_ibfk_2` FOREIGN KEY (`cliente_id`) REFERENCES `clientes` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pedidos_ibfk_3` FOREIGN KEY (`extension_id`) REFERENCES `extensiones` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pedidos_ibfk_4` FOREIGN KEY (`formato_id`) REFERENCES `formatos` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pedidos_ibfk_5` FOREIGN KEY (`idioma_id`) REFERENCES `idiomas` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pedidos_ibfk_6` FOREIGN KEY (`modalidad_id`) REFERENCES `modalidades` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pedidos_ibfk_7` FOREIGN KEY (`tipo_id`) REFERENCES `tipos` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pedidos_ibfk_8` FOREIGN KEY (`proposito_id`) REFERENCES `propositos` (`id`);

--
-- Filtros para la tabla `pedidos_categorias`
--
ALTER TABLE `pedidos_categorias`
  ADD CONSTRAINT `pedidos_categorias_ibfk_1` FOREIGN KEY (`pedido_id`) REFERENCES `pedidos` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pedidos_categorias_ibfk_2` FOREIGN KEY (`categoria_id`) REFERENCES `categorias` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `pedidos_paises`
--
ALTER TABLE `pedidos_paises`
  ADD CONSTRAINT `pedidos_paises_ibfk_1` FOREIGN KEY (`pedido_id`) REFERENCES `pedidos` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pedidos_paises_ibfk_2` FOREIGN KEY (`pais_id`) REFERENCES `paises` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `perspectivas`
--
ALTER TABLE `perspectivas`
  ADD CONSTRAINT `perspectivas_ibfk_1` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `propositos`
--
ALTER TABLE `propositos`
  ADD CONSTRAINT `propositos_ibfk_1` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `tipos`
--
ALTER TABLE `tipos`
  ADD CONSTRAINT `tipos_ibfk_1` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `usuarios_tipos`
--
ALTER TABLE `usuarios_tipos`
  ADD CONSTRAINT `usuarios_tipos_ibfk_1` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
