<?php

namespace app\controllers;

use Yii;
use app\models\Pedidos;
use app\models\Status;
use app\models\Formatos;
use app\models\Tipos;
use app\models\Categorias;
use app\models\Modalidades;
use app\models\Propositos;
use app\models\Paises;
use app\models\Extensiones;
use app\models\Idiomas;
use app\models\Perspectivas;
use app\models\Clientes;
use app\models\PedidosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PedidosController implements the CRUD actions for Pedidos model.
 */
class PedidosController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Pedidos models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PedidosSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $listStatus=Status::listStatus();
        $listClientes=Clientes::listClientes();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'listStatus' => $listStatus,
            'listClientes' => $listClientes
        ]);
    }

    /**
     * Displays a single Pedidos model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Pedidos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Pedidos();

        $listFormatos=Formatos::listFormatos();
        $listTipos=Tipos::listTipos();
        $listCategorias=Categorias::listCategorias();
        $listModalidades=Modalidades::listModalidades();
        $listPropositos=Propositos::listPropositos();
        $listExtensiones=Extensiones::listExtensiones();
        $listPaises=Paises::listPaises();
        $listIdiomas=Idiomas::listIdiomas();
        $listPerspectivas=Perspectivas::listPerspectivas();
        $listOptimizacion=[1=>'Si',0=>'No'];
        $listClientes=[];

        if(Yii::$app->user->identity->tipoUsuario == 1){
            $listClientes=Clientes::listClientes();
        }else{
            $clienteConectado=Clientes::clienteConectado();
            $model->cliente_id=$clienteConectado['id'];
            $model->cliente_nombre=$clienteConectado['primer_apellido']." ".$clienteConectado['primer_nombre'];
        }


        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $registro=$model->registrarPedido(Yii::$app->request->post());

            if($registro){
                return $this->redirect(['view', 'id' => $model->id]);
            }else{
                return $this->redirect(['site/error']);
            }  
        }

        return $this->render('create', [
            'model' => $model,
            'listFormatos' => $listFormatos,
            'listTipos' => $listTipos,
            'listCategorias' => $listCategorias,
            'listModalidades' => $listModalidades,
            'listPropositos' => $listPropositos,
            'listExtensiones' => $listExtensiones,
            'listPaises' => $listPaises,
            'listIdiomas' => $listIdiomas,
            'listOptimizacion' => $listOptimizacion,
            'listClientes' => $listClientes,
            'listPerspectivas' => $listPerspectivas
        ]);
    }

    /**
     * Updates an existing Pedidos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->categorias=$model->categoriasSeleccionadas();
        $model->paises=$model->paisesSeleccionados();

        $listFormatos=Formatos::listFormatos();
        $listTipos=Tipos::listTipos();
        $listCategorias=Categorias::listCategorias();
        $listModalidades=Modalidades::listModalidades();
        $listPropositos=Propositos::listPropositos();
        $listPerspectivas=Perspectivas::listPerspectivas();
        $listExtensiones=Extensiones::listExtensiones();
        $listPaises=Paises::listPaises();
        $listIdiomas=Idiomas::listIdiomas();
        $listOptimizacion=[1=>'Si',0=>'No'];
        $listClientes=[];

        if(Yii::$app->user->identity->tipoUsuario == 1){
            $listClientes=Clientes::listClientes();
        }else{
            $clienteConectado=Clientes::clienteConectado();
            $model->cliente_id=$clienteConectado['id'];
            $model->cliente_nombre=$clienteConectado['primer_apellido']." ".$clienteConectado['primer_nombre'];
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $registro=$model->modificarPedido(Yii::$app->request->post());

            if($registro){
                return $this->redirect(['view', 'id' => $model->id]);
            }else{
                return $this->redirect(['site/error']);
            }  
        }


        /*echo "<pre>";
        print_r(Yii::$app->request->post());
        echo "</pre>";
        die;*/

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $registro=$model->modificarPedido(Yii::$app->request->post());

            if($registro){
                return $this->redirect(['view', 'id' => $model->id]);
            }else{
                return $this->redirect(['site/error']);
            }

            
        }

        $model->fecha_entrega=date("d/m/Y",strtotime($model->fecha_entrega));

        return $this->render('update', [
            'model' => $model,
            'listFormatos' => $listFormatos,
            'listTipos' => $listTipos,
            'listCategorias' => $listCategorias,
            'listModalidades' => $listModalidades,
            'listPropositos' => $listPropositos,
            'listPerspectivas' => $listPerspectivas,
            'listExtensiones' => $listExtensiones,
            'listPaises' => $listPaises,
            'listIdiomas' => $listIdiomas,
            'listOptimizacion' => $listOptimizacion,
            'listClientes' => $listClientes
        ]);
    }

    /**
     * Deletes an existing Pedidos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionDeactivate($id){
        $model = $this->findModel($id);
        $model->categorias=1;
        $model->paises=1;
        $model->status_id=2;
        $model->save();
        return $this->redirect(['view', 'id' => $model->id]);
    }

    public function actionActivate($id){
        $model = $this->findModel($id);
        $model->categorias=1;
        $model->paises=1;
        $model->status_id=1;
        $model->save();
        return $this->redirect(['view', 'id' => $model->id]);
    }

    /**
     * Finds the Pedidos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pedidos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pedidos::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
