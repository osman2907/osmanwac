<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'css/site.css',
        'css/bootstrap.min.css',
        'css/font-awesome.min.css',
        'css/themify-icons.css',
        'css/metisMenu.css',
        'css/owl.carousel.min.css',
        'css/slicknav.min.css',
        'css/typography.css',
        'css/default-css.css',
        'css/styles.css',
        'css/responsive.css',
        'css/fastselect.min.css'
    ];
    public $js = [
        //'js/jquery-2.2.4.min.js',
        'js/popper.min.js',
        'js/bootstrap.min.js',
        'js/owl.carousel.min.js',
        'js/metisMenu.min.js',
        'js/jquery.slimscroll.min.js',
        'js/jquery.slicknav.min.js',
        'js/plugins.js',
        'js/scripts.js',
        'js/fastselect.standalone.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
